package main

import (
	"log"
	"math"
	"os"
	"strconv"
)

const (
	N              = 69
	Fd             = 44000.0
	FdInt          = 44000
	Fc             = 30000.0
	Fs             = 20000.0
	filenameOutput = "result.dat"
	smallValue     = 0.000001
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to filter function */
func funcFilter(t float64) float64 {

	/* return value */
	//if math.Abs(t) <= smallValue {
	//	return 2 * Fs
	//}
	//return math.Sin(2.0 * math.Pi * Fs * t) / (t * math.Pi)

	return math.Sin(2.0 * math.Pi * 5000.0 * t) +
		2.0 * math.Sin(2.0 * math.Pi * 15000.0 * t)

}

/* function to do filter (low-pass) */
func doFilter(in []float64, out *[]float64, sizeIn int) {

	var (
		H     []float64
		H_id  []float64
		W     []float64
		i, j  int
		normC float64
	)

	/* initialization */
	H = make([]float64, N)
	H_id = make([]float64, N)
	W = make([]float64, N)
	i = 0
	for i < N {
		H[i] = 0.0
		H_id[i] = 0.0
		W[i] = 0.0
		i++
	}

	/* values of FIR-filter */
	i = 0
	for i < N {
		if (i - N / 2) == 0 {
			H_id[i] = 1.0
		} else {
			H_id[i] = math.Sin(2 * Fc * float64(i - N / 2) / Fd) / (2 * Fc * float64(i - N / 2) / Fd)
		}
		W[i] = 0.5 + 0.5 * math.Cos(math.Pi * float64(i - N / 2) * Fc / (float64(N) * Fd))
		H[i] = H_id[i] * W[i]
		i++
	}

	/* normalize coefficeints */
	normC = 0.0
	i = 0
	for i < N {
		normC += H[i]
		i++
	}
	i = 0
	for i < N {
		H[i] /= normC
		i++
	}

	/* calculate filter function */
	i = 0
	for i < sizeIn {
		(*out)[i] = 0.0
		j = 0
		for j < N - 1 {
			if  i - j >= 0 {
				(*out)[i] += H[j] * in[i - j]
			} else {
				break
			}
			j++
		}
		i++
	}

}

/* entry point */
func main() {

	var (
		t          []float64
	    in         []float64
	 	out        []float64
		i          int
		err        error
		fileOutput *os.File
	)

	/* create arrays */
	t = make([]float64, FdInt)
	in = make([]float64, FdInt)
	out = make([]float64, FdInt)

	/* initialize arrays */
	i = 0
	for i < FdInt {
		t[i] = float64(i) / Fd
		in[i] = funcFilter(t[i])
		i++
	}

	/* do filter (low-pass) */
	doFilter(in, &out, FdInt)

	/* create file */
	fileOutput, err = os.Create(filenameOutput)
	handleError(err)

	/* write header */
	_, err = fileOutput.WriteString("#in out t\n")
	handleError(err)

	/* write results */
	i = 0
	for i < FdInt {
		_, err = fileOutput.WriteString(strconv.FormatFloat(in[i], 'f', -1, 64) + " " +
			strconv.FormatFloat(out[i], 'f', -1, 64) + " " +
			strconv.FormatFloat(t[i], 'f', -1, 64) + "\n")
		handleError(err)
		i++
	}

}